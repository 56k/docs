![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Example [56K.Cloud] Documentation website using GitLab Pages.

---


## Developer Onboarding

[56K.Cloud]: https://56k.cloud
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages